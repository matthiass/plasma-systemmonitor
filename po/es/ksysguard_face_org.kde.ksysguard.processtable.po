# Spanish translations for ksysguard_face_org.kde.ksysguard.processtable.po package.
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the plasma-systemmonitor package.
#
# Automatically generated, 2020.
# Eloy Cuadra <ecuadra@eloihr.net>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: ksysguard_face_org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-06-26 00:19+0000\n"
"PO-Revision-Date: 2021-05-27 01:00+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.1\n"

#: src/faces/processtable/contents/ui/Config.qml:26
#, kde-format
msgctxt "@option:check"
msgid "Confirm ending processes."
msgstr "Confirmar la terminación de procesos."

#: src/faces/processtable/contents/ui/Config.qml:31
#, kde-format
msgctxt "@label:listbox"
msgid "By default show:"
msgstr "Mostrar por omisión:"

#: src/faces/processtable/contents/ui/Config.qml:40
#, kde-format
msgctxt "@item:inlistbox"
msgid "Own Processes"
msgstr "Procesos propios"

#: src/faces/processtable/contents/ui/Config.qml:41
#, kde-format
msgctxt "@item:inlistbox"
msgid "User Processes"
msgstr "Procesos de usuarios"

#: src/faces/processtable/contents/ui/Config.qml:42
#, kde-format
msgctxt "@item:inlistbox"
msgid "System Processes"
msgstr "Procesos del sistema"

#: src/faces/processtable/contents/ui/Config.qml:43
#, kde-format
msgctxt "@item:inlistbox"
msgid "All Processes"
msgstr "Todos los procesos"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:29
#, kde-format
msgctxt "@action"
msgid "Search"
msgstr "Buscar"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:40
#, kde-format
msgctxt "@action"
msgid "End Process"
msgstr "Terminar proceso"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:54
#, kde-format
msgctxt "@action"
msgid "Display as List"
msgstr "Mostrar como lista"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:71
#, kde-format
msgctxt "@action"
msgid "Display as Tree"
msgstr "Mostrar como árbol"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:83
#, kde-format
msgctxt "@action %1 is view type"
msgid "Show: %1"
msgstr "Mostrar: %1"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:86
#, kde-format
msgctxt "@item:inmenu"
msgid "Own Processes"
msgstr "Procesos propios"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:95
#, kde-format
msgctxt "@item:inmenu"
msgid "User Processes"
msgstr "Procesos de usuarios"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:104
#, kde-format
msgctxt "@item:inmenu"
msgid "System Processes"
msgstr "Procesos del sistema"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:113
#, kde-format
msgctxt "@item:inmenu"
msgid "All Processes"
msgstr "Todos los procesos"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:125
#, kde-format
msgctxt "@action"
msgid "Configure columns…"
msgstr "Configurar columnas..."

#: src/faces/processtable/contents/ui/FullRepresentation.qml:173
#, kde-format
msgctxt "@action:inmenu"
msgid "Send Signal"
msgstr "Enviar señal"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:176
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Suspend (STOP)"
msgstr "Suspender (STOP)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:180
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Continue (CONT)"
msgstr "Continuar (CONT)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:184
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Hangup (HUP)"
msgstr "Detener (HUP)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:188
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Interrupt (INT)"
msgstr "Interrumpir (INT)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:192
#, kde-format
msgctxt "@action:inmenu  Send Signal"
msgid "Terminate (TERM)"
msgstr "Terminar (TERM)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:196
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Kill (KILL)"
msgstr "Matar (KILL)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:200
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "User 1 (USR1)"
msgstr "Usuario 1 (USR1)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:204
#, kde-format
msgctxt "@action:inmenu  Send Signal"
msgid "User 2 (USR2)"
msgstr "Usuario 2 (USR2)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:212
#, kde-format
msgctxt "@action:inmenu"
msgid "End Process"
msgid_plural "End %1 Processes"
msgstr[0] "Terminar proceso"
msgstr[1] "Terminar %1 procesos"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:224
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Columns…"
msgstr "Configurar columnas..."

#: src/faces/processtable/contents/ui/FullRepresentation.qml:234
#, kde-format
msgctxt "@title:window"
msgid "End Process"
msgid_plural "End %1 Processes"
msgstr[0] "Terminar proceso"
msgstr[1] "Terminar %1 procesos"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:235
#, kde-format
msgctxt "@action:button"
msgid "End"
msgstr "Terminar"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:237
#, kde-format
msgid ""
"Are you sure you want to end this process?\n"
"Any unsaved work may be lost."
msgid_plural ""
"Are you sure you want to end these %1 processes?\n"
"Any unsaved work may be lost."
msgstr[0] ""
"¿Seguro que quiere terminar este proceso?\n"
"Se perderá el trabajo no guardado."
msgstr[1] ""
"¿Seguro que quiere terminar estos %1 procesos?\n"
"Se perderá el trabajo no guardado."

#: src/faces/processtable/contents/ui/FullRepresentation.qml:244
#, kde-format
msgctxt "@item:intable %1 is process id, %2 is user name"
msgid "Process ID %1, owned by %2"
msgstr "ID del proceso %1, propiedad de %2"
